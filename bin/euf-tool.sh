#!/bin/sh

EUF_TOOL_NAME="euf-tool-impl.sh"
EUF_TOOL_IMPL_BIN=".$EUF_TOOL_NAME"
EUF_TOOL_IMPL_URL="https://bitbucket.org/josekitech/euf-tool-seed/raw/HEAD/bin/$EUF_TOOL_NAME"

# update to latest impl
wget -q $EUF_TOOL_IMPL_URL -O $EUF_TOOL_IMPL_BIN

chown $USER $EUF_TOOL_IMPL_BIN
chmod 755 $EUF_TOOL_IMPL_BIN
./$EUF_TOOL_IMPL_BIN $@
