#!/bin/sh
#
#
# https://stackoverflow.com/questions/16483119/example-of-how-to-use-getopts-in-bash
VERSION="1.0.0"
declare -i VALIDATION_FALIURES=0
DATE_STAMP=`date +%Y%m%d`

# NPM_PACKAGES=() # leaving to ease testing this script
NPM_PACKAGES=(
copyfiles@1.2.0
flow-bin@0.52.0
flow-copy-source@1.2.0
generator-electrode@3.3.0
generator-electrode-component@1.4.0
jasmine@2.7.0
n@2.1.8
npm@3.10.10
npm-check@5.4.5
npm-check-updates@2.12.1
npmrc@1.1.1
pnpm@1.10.2
rimraf@2.6.1
xclap-cli@0.1.2
)

# NPM_PACKAGES_FOR_REMOVAL=() # leaving to ease testing this script
NPM_PACKAGES_FOR_REMOVAL=(
copyfiles
flow-bin
flow-copy-source
generator-electrode
generator-electrode-component
jasmine
n
npm
npm-check
npm-check-updates
npmrc
pnpm
rimraf
xclap-cli
)

usage() {
  echo "euf-tool-impl version $VERSION"
  echo ""
  echo "Usage: $0 [OPTION]"
  echo ""
  echo "-c\t Configure/updated your develpment environment"
  echo "-v\t Validate your development environment"
  echo "-n\t Configure NPM (included with -c)"
  echo "-a\t Configure Artifactory (included with -c)"
  echo "-r\t Remove it all and start over"
  echo "-h\t Shows this usage"
}

reset() {
  echo "Resetting your environment ..."

  # reset .npmrc to public npm repo
  cp ~/.npmrc ~/.npmrc.${DATE_STAMP} # backup existing .npmrc
  rm -rf ~/.npmrc
  echo "registry=https://registry.npmjs.org/" > ~/.npmrcs/default

  # yank out the npm packages we installed
  echo "\tRemoving global NPM packages. Be patient this can take a bit ..."
  for package in ${NPM_PACKAGES_FOR_REMOVAL[*]}; do
    npm --quiet i -g $package > /dev/null 2>&1
  done

  echo "\tEnvironment reset completed successfully!"
}

configure_npm() {
  echo "Installing global NPM packages. Be patient this can take a bit ..."
  for package in ${NPM_PACKAGES[*]}; do
    npm --quiet i -g $package > .configure_npm-install.log 2>&1
    echo ".\c"
  done
  echo "\tAll global NPM modules successfully installed!"
}

configure_artifactory() {
  echo "Configuring Artifactory ..."
  echo "Enter your network username:>\c"
  read username
  echo "Enter your password:>\c"
  read password

  ARTIFACTORY_NPM_URL="http://zrepoqa.staples.com/artifactory/api/npm"
  curl -f -s -u$username:$password $ARTIFACTORY_NPM_URL/auth > ~/.npmrcs/staples
  if [ $? != 0 ]; then
    exit $?
  fi
  curl -f -s -u$username:$password $ARTIFACTORY_NPM_URL/staples-virtual-npm-repo/auth/euf >> ~/.npmrcs/staples
  if [ $? != 0 ]; then
    exit $?
  fi
  npmrc staples > /dev/null 2&>1
  echo "\tArtifactory successfully configured!"
}

# sets up the users environment for development
configure() {
  echo "Configuring your development environment ..."
  mkdir -p ~/devel/staples/euf
  echo "\tPlease checkout all EUF repos to ~/devel/staples/euf"
  configure_npm
  configure_artifactory
  n 8.2.1
}

# helper routine for actually performing the validation checks
check() {
  $($3)
  TEST_EXIT_STATUS=$?
  if [ $TEST_EXIT_STATUS != 0 ]; then
    VALIDATION_FALIURES+=1
  fi
  echo "\t$1\t[$([ $TEST_EXIT_STATUS == 0 ] && echo "PASS" || echo "FAIL : $2")]"
}

# Ensures the users environment is sane for development
validate() {
  echo "Validating local development environment ..."
  declare -i RESULT=0
  UNAME=`uname`
  check "OS\t" "Deteced OS $UNAME. Only OSX is supported!" "test $UNAME = Darwin"
  check "GIT\t" "Please install git!" "test -x $(which git)"
  check "NODE\t" "Please install node!" "test -x $(which node)"
  NODE_VERSION=`node -v`
  check "NODE VERSION" "Detected NodeJS version $NODE_VERSION but, v8.2.1 is required!" "test $NODE_VERSION = v8.2.1"
  NPM_VERSION=`npm -v`
  check "NPM VERSION" "Detected NodeJS version $NPM_VERSION but, v5.3.1 0is required!" "test $NPM_VERSION = 5.3.0"

  # echo "VALIDATION_FALIURES=[$VALIDATION_FALIURES]"
  if [ $VALIDATION_FALIURES != 0 ]; then
    echo "*** There were $VALIDATION_FALIURES validation error(s) but, don't freak yet. Try executing the following cmd and then validating again. ***"
    echo "./euf-tool-impl.sh -c"
  fi
}

while getopts "cvnarh" o; do
    # echo "o=>:[${o}]"
    case "${o}" in
        c)
            configure
            ;;
        v)
            validate
            ;;
        n)
            configure_npm
            ;;
        a)
            configure_artifactory
            ;;
        r)
            reset
            ;;
        *|h)
            usage
            ;;
    esac
done
#shift $((OPTIND-1))

if [ -z "$@" ]; then
    usage
fi
